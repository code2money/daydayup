package com.lecoboy.leecode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RemoveDuplicatesInts {
    public static void main(String[] args) {
        int[] i = new int[]{0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        System.out.println("总条数" + removeDuplicates(i));
    }

    public int removeDuplicates(int[] nums) {
        if (nums.length == 0) return 0;
        Set<Integer> set = new HashSet<>();
        for (int j = 0; j < nums.length; j++) {
            set.add(nums[j]);
        }
        return set.size();
    }

    private static void printNums(int[] nums) {
        System.out.println("---");
        for (int i : nums) {
            System.out.printf(i + ",");
        }

    }

    public static int removeDuplicates2(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        //慢指针，用于存放相同key的索引位置
        int i = 0;
        //定义j从第2个开始起始，不断和i指针数据对比
        //如果不相等则向下移动对比值指针
        //并且把对比的下一个数据挪到对比指针下一个指针位置即nums[i]，后面就可以继续使用nums[i]进行对比
        for (int j = 1; j < nums.length; j++) {
            if (nums[j] != nums[i]) {
                i++;
                nums[i] = nums[j];
            }
        }
        //因为从第一位起就应该算一个不重复数据
        return i + 1;
    }
}
