package com.lecoboy.designpatterns;

import com.lecoboy.designpatterns.strategy.demo01.ICalculator;
import com.lecoboy.designpatterns.strategy.demo01.impl.Minus;
import com.lecoboy.designpatterns.strategy.demo01.impl.Multiply;
import com.lecoboy.designpatterns.strategy.demo01.impl.Plus;
import com.lecoboy.designpatterns.strategy.demo02.IQuoteStrategy;
import com.lecoboy.designpatterns.strategy.demo02.QuoteContext;
import com.lecoboy.designpatterns.strategy.demo02.impl.NewCustomerQuoteStrategy;
import com.lecoboy.designpatterns.strategy.demo02.impl.OldCustomerQuoteStrategy;
import com.lecoboy.designpatterns.strategy.demo02.impl.VipCustomerQuoteStrategy;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * 策略模式测试类
 */
public class StrategyTest {
    //TODO 策略模式

    /**
     * 加减乘算法策略模式
     * 策略模式定义了一系列算法，并将每个算法封装起来，使他们可以相互替换，且算法的变化不会影响到使用算法的客户
     * <p>
     * 策略模式的决定权在用户，系统本身提供不同算法的实现，新增或者删除算法，对各种算法做封装。
     * 因此，策略模式多用在算法决策系统中，外部用户只需要决定用哪个算法即可。
     * <p>
     * 策略模式和工厂模式的区别 https://www.cnblogs.com/me115/p/3790615.html
     */
    @Test
    public void demo01Test() {
        //加
        String exp = "2+8";
        ICalculator cal = new Plus();
        int result = cal.calculate(exp);
        System.out.println(result);

        //减
        exp = "2-8";
        cal = new Minus();
        result = cal.calculate(exp);
        System.out.println(result);

        //乘
        exp = "2*8";
        cal = new Multiply();
        result = cal.calculate(exp);
        System.out.println(result);
    }

    @Test
    public void demo02Test() {
        //1.创建老客户的报价策略
        IQuoteStrategy oldQuoteStrategy = new OldCustomerQuoteStrategy();
        //2.创建报价上下文对象，并设置具体的报价策略
        QuoteContext quoteContext = new QuoteContext(oldQuoteStrategy);
        //3.调用报价上下文的方法
        BigDecimal price = quoteContext.getPrice(new BigDecimal(100));
        System.out.println("折扣价为：" + price);


        //1.创建新客户的报价策略
        IQuoteStrategy newQuoteStrategy = new NewCustomerQuoteStrategy();
        //2.创建报价上下文对象，并设置具体的报价策略
        quoteContext = new QuoteContext(newQuoteStrategy);
        //3.调用报价上下文的方法
        price = quoteContext.getPrice(new BigDecimal(100));
        System.out.println("折扣价为：" + price);


        //1.创建VIP客户的报价策略
        IQuoteStrategy vipQuoteStrategy = new VipCustomerQuoteStrategy();
        //2.创建报价上下文对象，并设置具体的报价策略
        quoteContext = new QuoteContext(vipQuoteStrategy);
        //3.调用报价上下文的方法
        price = quoteContext.getPrice(new BigDecimal(100));
        System.out.println("折扣价为：" + price);
    }
}
