package com.lecoboy.designpatterns.factory;

import com.lecoboy.designpatterns.factory.impl.MailSender;
import com.lecoboy.designpatterns.factory.impl.SmsSender;

/**
 * @description: 发送消息工厂
 * 普通工厂模式，就是建立一个工厂类，对实现了同一接口的一些类进行
 * @author: lizy
 * @date: 2019/2/1
 */
public class SendFactory {
    public enum SenderType{
        SMS,EMAIl
    }

    /**
     * 生产消息
     * @param senderType
     * @return
     */
    public Sender produce(SenderType senderType){
        if(senderType.equals(SenderType.SMS)){
            return new SmsSender();
        }else if(senderType.equals(SenderType.EMAIl)){
            return new MailSender();
        }else {
            System.out.println("请输入正确的类型");
            return null;
        }
    }
}
