package com.lecoboy.designpatterns.factory;

import com.lecoboy.designpatterns.factory.impl.MailSender;
import com.lecoboy.designpatterns.factory.impl.SmsSender;

/**
 * @description: 多个工厂方法模式
 * @author: lizy
 * @date: 2019/2/1
 */
public class SendFactory2 {
    //生产mail
    public Sender produceMail() {
        return new MailSender();
    }

    //生产sms
    public Sender produceSms() {
        return new SmsSender();
    }
    // 需要就继续往下罗列增加...
}

