package com.lecoboy.designpatterns.factory;

import com.lecoboy.designpatterns.factory.impl.MailSender;
import com.lecoboy.designpatterns.factory.impl.SmsSender;

/**
 * @description: 静态工厂方法模式
 * @author: lizy
 * @date: 2019/2/1
 */
public class SendFactory3 {
    public static Sender produceMail() {
        return new MailSender();
    }

    public static Sender produceSms() {
        return new SmsSender();
    }
}
