package com.lecoboy.designpatterns.factory;

/**
 * 发送(邮件和短信共同接口)
 *
 * @author: lizhenyu1
 * @create 2019-02-01 15:48
 */
public interface Sender {
    String Send();
}
