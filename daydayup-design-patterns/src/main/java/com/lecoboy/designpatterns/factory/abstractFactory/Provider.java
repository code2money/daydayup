package com.lecoboy.designpatterns.factory.abstractFactory;

import com.lecoboy.designpatterns.factory.Sender;

/**
 * 生产接口
 */
public interface Provider {
    Sender produce();
}
