package com.lecoboy.designpatterns.factory.abstractFactory;

import com.lecoboy.designpatterns.factory.Sender;
import com.lecoboy.designpatterns.factory.impl.MailSender;

/**
 * @description: 发送邮件工厂
 * @author: lizy
 * @date: 2019/2/1
 */
public class SendMailFactory implements Provider {
    @Override
    public Sender produce() {
        return new MailSender();
    }
}
