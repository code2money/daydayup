package com.lecoboy.designpatterns.factory.abstractFactory;
import com.lecoboy.designpatterns.factory.Sender;
import com.lecoboy.designpatterns.factory.impl.SmsSender;

/**
 * 短信 抽象工厂
 */
public class SendSmsFactory implements Provider {
    @Override
    public Sender produce() {
        return new SmsSender();
    }
}
