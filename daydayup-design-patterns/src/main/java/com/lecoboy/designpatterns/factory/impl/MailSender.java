package com.lecoboy.designpatterns.factory.impl;

import com.lecoboy.designpatterns.factory.Sender;

/**
 * @description: 邮件实现类
 * @author: lizy
 * @date: 2019/2/1
 */
public class MailSender implements Sender {
    @Override
    public String Send() {
        System.out.println("this is mail sender!");
        return "ok";
    }
}
