package com.lecoboy.designpatterns.factory.impl;

import com.lecoboy.designpatterns.factory.Sender;

/**
 * @description: 短信实现类
 * @author: lizy
 * @date: 2019/2/1
 */
public class SmsSender implements Sender {
    @Override
    public String Send() {
        System.out.println("this is sms sender!");
        return "ok";
    }
}
