package com.lecoboy.designpatterns.strategy.demo01;

/**
 * 辅助抽象类 可有可无
 */
public abstract class AbstractCalculator {
    //分割字符串 返回int数组
    public int[] split(String exp,String opt){
        String array[] = exp.split(opt);
        int arrayInt[] = new int[2];
        arrayInt[0] = Integer.parseInt(array[0]);
        arrayInt[1] = Integer.parseInt(array[1]);
        return arrayInt;
    }
}
