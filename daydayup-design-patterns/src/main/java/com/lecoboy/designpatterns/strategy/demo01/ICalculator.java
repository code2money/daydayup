package com.lecoboy.designpatterns.strategy.demo01;

/**
 * 定义个统一接口  计算器
 */
public interface ICalculator {
    int calculate(String exp);
}
