package com.lecoboy.designpatterns.strategy.demo01.impl;

import com.lecoboy.designpatterns.strategy.demo01.AbstractCalculator;
import com.lecoboy.designpatterns.strategy.demo01.ICalculator;

/**
 * 减法算法 策略
 */
public class Minus extends AbstractCalculator implements ICalculator {
    @Override
    public int calculate(String exp) {
        int[] arry = split(exp, "-");
        return arry[0] - arry[1];
    }
}
