package com.lecoboy.designpatterns.strategy.demo01.impl;

import com.lecoboy.designpatterns.strategy.demo01.AbstractCalculator;
import com.lecoboy.designpatterns.strategy.demo01.ICalculator;

/**
 * 加法算法策略
 */
public class Plus extends AbstractCalculator implements ICalculator {
    @Override
    public int calculate(String exp) {
        int[] arry = split(exp, "\\+");
        return arry[0] + arry[1];
    }
}
