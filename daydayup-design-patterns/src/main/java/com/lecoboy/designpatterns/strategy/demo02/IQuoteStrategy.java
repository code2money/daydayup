package com.lecoboy.designpatterns.strategy.demo02;

import java.math.BigDecimal;

/**
 * 报价策略接口
 */
public interface IQuoteStrategy {
    //获取折扣后的价格
    BigDecimal getPrice(BigDecimal originalPrice);
}
