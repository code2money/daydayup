package com.lecoboy.designpatterns.strategy.demo02;

import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

/**
 * 策略上下文
 */
public class QuoteContext {

    //持有一个具体的报价策略
    @Autowired
    private IQuoteStrategy iQuoteStrategy;

    //注入报价策略
    public QuoteContext(IQuoteStrategy quoteStrategy) {
        this.iQuoteStrategy = quoteStrategy;
    }

    //回调具体报价策略的方法
    public BigDecimal getPrice(BigDecimal originalPrice) {
        return iQuoteStrategy.getPrice(originalPrice);
    }

}
