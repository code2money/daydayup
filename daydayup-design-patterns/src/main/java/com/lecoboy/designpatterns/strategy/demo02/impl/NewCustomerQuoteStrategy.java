package com.lecoboy.designpatterns.strategy.demo02.impl;

import com.lecoboy.designpatterns.strategy.demo02.IQuoteStrategy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 新客户报价策略实现类
 */
@Service
public class NewCustomerQuoteStrategy implements IQuoteStrategy {
    @Override
    public BigDecimal getPrice(BigDecimal originalPrice) {
        System.out.println("抱歉！新客户没有折扣！");
        return originalPrice;
    }
}
