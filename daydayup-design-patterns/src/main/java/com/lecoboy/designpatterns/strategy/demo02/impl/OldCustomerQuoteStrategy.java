package com.lecoboy.designpatterns.strategy.demo02.impl;

import com.lecoboy.designpatterns.strategy.demo02.IQuoteStrategy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 老客户折扣实现类
 */
@Service
public class OldCustomerQuoteStrategy implements IQuoteStrategy {
    @Override
    public BigDecimal getPrice(BigDecimal originalPrice) {
        System.out.println("恭喜！老客户享有9折优惠！");
        //进行四舍五入
        originalPrice = originalPrice.multiply(new BigDecimal(0.9).setScale(2, BigDecimal.ROUND_HALF_UP));
        return originalPrice;
    }
}
