package com.lecoboy.designpatterns.strategy.demo02.impl;

import com.lecoboy.designpatterns.strategy.demo02.IQuoteStrategy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * VIP折扣实现类
 */
@Service
public class VipCustomerQuoteStrategy implements IQuoteStrategy {
    @Override
    public BigDecimal getPrice(BigDecimal originalPrice) {
        System.out.println("恭喜！VIP客户享有8折优惠！");
        originalPrice = originalPrice.multiply(new BigDecimal(0.8)).setScale(2, BigDecimal.ROUND_HALF_UP);
        return originalPrice;
    }
}
