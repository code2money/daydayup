### 一、设计模式的分类
- 总体来说设计模式分为三大类：
- 创建型模式，共五种：工厂方法模式、抽象工厂模式、单例模式、建造者模式、原型模式。
- 结构型模式，共七种：适配器模式、装饰器模式、代理模式、外观模式、桥接模式、组合模式、享元模式。
- 行为型模式，共十一种：策略模式、模板方法模式、观察者模式、迭代子模式、责任链模式、命令模式、备忘录模式、状态模式、访问者模式、中介者模式、解释器模式。
- 其实还有两类：并发型模式和线程池模式

[设计模式关系](https://img-my.csdn.net/uploads/201211/29/1354152786_2930.jpg)

### 二、六大原则
- 1.开闭原则
- 2.里氏替换原则
- 3.依赖倒转原则
- 4.接口隔离原则
- 5.迪米特法则（最少知道原则）
- 6.合成复用原则

### 三、Java中的23种设计模式
[CSDN链接](https://blog.csdn.net/zhangerqing/article/details/8194653)
