package com.lecoboy.gupaoclass.principle.dependencyinversion;

/**
 * 依赖倒置原则
 *
 * 高层模块
 * 不应该依赖低层，而是应该其抽象
 * 底层写过后，一般不会变，改变的在高层
 * 面向抽象编程
 */
public class DipTest {
    public static void main(String[] args) {
        //---------v1  面向实现编程----------
       /* Tom tom = new Tom();
        tom.studyAiCourse();
        tom.studyJavaCourse();
        tom.studyPythonCourse();*/

        //---------v2 注入方式实现依赖倒置---------
        // 符合开闭原则，Spring中常见，即依赖注入
       /* Tom tom = new Tom();
        tom.study(new JavaCourse());
        tom.study(new PythonCourse());*/

        //---------v3 构造方法实现依赖倒置---------
      /*  Tom tom = new Tom(new JavaCourse());
        tom.study();
*/

        //---------v4 构造方法的升级版 在Tom单例时多设置---------
        Tom tom = new Tom();
        tom.setCourse(new JavaCourse());
        tom.study();

        tom.setCourse(new PythonCourse());
        tom.study();

    }
}
