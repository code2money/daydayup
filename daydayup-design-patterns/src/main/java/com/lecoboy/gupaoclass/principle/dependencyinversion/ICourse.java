package com.lecoboy.gupaoclass.principle.dependencyinversion;

/**
 *
 * 课程抽象方法
 * 面向接口编程
 */
public interface ICourse {
    void study();
}
