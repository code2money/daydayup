package com.lecoboy.gupaoclass.principle.dependencyinversion;

/**
 * Java抽象类
 */
public class JavaCourse implements ICourse {

    @Override
    public void study() {
        System.out.println("Tom正在学习java");
    }
}
