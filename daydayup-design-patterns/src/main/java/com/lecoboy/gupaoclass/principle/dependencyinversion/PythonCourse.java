package com.lecoboy.gupaoclass.principle.dependencyinversion;

/**
 * python抽象类
 */
public class PythonCourse implements ICourse {
    @Override
    public void study() {
        System.out.println("Tom正在学习Python");
    }
}
