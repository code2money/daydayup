package com.lecoboy.gupaoclass.principle.dependencyinversion;

public class Tom {
   /*
   -------v1------
   public void studyJavaCourse() {
        System.out.println("Tom正在学习java");
    }

    public void studyPythonCourse() {
        System.out.println("Tom正在学习Python");
    }

    public void studyAiCourse() {
        System.out.println("Tom正在学习Ai");
    }*/


    /*-------v2------*/
    /* */
    /**
     * 符合开闭原则的 Tom的学习方法，其他的不需要关心
     *
     * @param iCourse 抽象的类
     *//*
   public void study(ICourse iCourse){
       iCourse.study();
   }*/
    /*-------v3 构造设置值------*/
    /*private ICourse iCourse;

    public Tom(ICourse iCourse) {
        this.iCourse = iCourse;
    }

    public void study() {
        iCourse.study();
    }*/

    /*-------v4 单例时可重新set值------*/
    private ICourse iCourse;

    public void setCourse(ICourse iCourse) {
        this.iCourse = iCourse;
    }

    public void study() {
        iCourse.study();
    }
}
