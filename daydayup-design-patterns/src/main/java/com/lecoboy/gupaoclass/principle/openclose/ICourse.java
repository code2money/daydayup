package com.lecoboy.gupaoclass.principle.openclose;

/**
 * 课程接口  面向接口编程
 */
public interface ICourse {
    Integer getId();

    String getName();

    Double getPrice();
}
