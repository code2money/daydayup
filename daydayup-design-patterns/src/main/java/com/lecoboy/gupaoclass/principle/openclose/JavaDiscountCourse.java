package com.lecoboy.gupaoclass.principle.openclose;

/**
 * 打折类
 */
public class JavaDiscountCourse extends JavaCourse {
    public JavaDiscountCourse(Integer id, String name, Double price) {
        super(id, name, price);
    }

    /**
     * 打折价格  里氏替换原则
     * @return
     */
    public Double getDiscountPrice() {
        //打八折
        return super.getPrice() * 0.8;
    }
}
