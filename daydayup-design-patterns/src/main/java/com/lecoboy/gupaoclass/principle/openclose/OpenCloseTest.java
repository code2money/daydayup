package com.lecoboy.gupaoclass.principle.openclose;


/**
 * 开闭原则
 */
public class OpenCloseTest {
    public static void main(String[] args) {
        ICourse iCourse = new JavaDiscountCourse(1, "《Java架构师》", 18000D);
        JavaDiscountCourse discountCourse = (JavaDiscountCourse) iCourse;
        System.out.println("Id：" + discountCourse.getId()
                + "\n 课程名称：" + discountCourse.getName()
                + "\n 折后价格：" + discountCourse.getDiscountPrice()
                + "\n 售价：" + discountCourse.getPrice());
    }
}
