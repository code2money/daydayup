### Java并发编程

#### 什么是线程
```
现代操作系统在运行一个程序时，会为其创建一个进程。
例如，启动一个Java程序，操作系统就会创建一个Java进程。现代操作系统调度的最小单位是线程，
也叫轻量级进程，在一个进程里可以创建多个线程，这些线程都拥有各自的计数器、堆栈和局部变量等属性，并且能
够访问共享的内存变量。处理器在这些线程上高速切换，让使用者感觉到这些线程在同时执行。
```

jps
jstack 线程id >-/work/dump1
grep 'java.lang.Thread.State' dump1 | awk '{print $2$3$4%5}' |sort |uniq -c

#### CAS自旋锁
demo：`CasAutomicCounter.java`

#### 线程的状态
demo：`ThreadState.java`

| 状态名称      | 说明 |
| ------ | ------ |
| NEW | 初始状态，线程被构建，但是还没有调用start()方法 |
| RUNNABLE | 运行状态，Java线程将操作系统中的就绪和运行两种状态笼统地乘坐"运行中" |
| BLOCKED | 阻塞状态，表示线程阻塞于锁 |
| WAITING | 等待状态，表示线程进入等待状态，进入该状态表示当前线程需要等待其他线程做出一些特定的动作（通知或中断） |
| TIME_WAITING | 超时等待状态，该状态不同于WAITING，它是可以在指定的时间自行返回的 |
| TERMINATED | 终止状态，表示当前线程已经执行完毕 |

#### 线程操作
demo：`WaitNotify.java`、`TheadState.java`

| 操作    | 作用 |
| ------    | ------ |
| start()   | 启动线程 |
| join()    | 线程阻塞 |
| interrupt() | 中断线程 |
| notify()  | 通知一个在对象上等待的线程，使其从wait()方法返回，而返回的前提是该线程获取到了对象的锁 |
| notifyAll() | 通知所有等待在该对象上的线程 |
| wait()    | 调用该方法的线程进入WAITING状态，只有等待另外线程的通知或被中断才会返回，需要注意，调用wait()方法后，会释放对象的锁 |
| wait(long)        | 超时等待一段时间，这里的参数时间是毫秒，也就是等待长达n毫秒，如果没有通知就超时返回 |
| wait(long,int)    | 对于超时时间更细粒度的控制，可以达到纳秒 |

#### TheadLocal
可使用在AOP记录耗时，demo参考`TheadLocalDemo.java`

#### 一个简单的数据连接池示例
demo：`connectionDemo`文件夹

包含以下知识点：
> - 代理模式
> - 超时模式
> - CountDownLanch并发工具
> - 双向链表结构
> - 原子操作计数器


### Lock接口
`锁是用来控制多线程访问共享资源的方式，一个锁能够防止多个线程同时访问共享资源
`
##### Lock接口提供的synchronized关键字不具备的主要特性

|特性|描述 |
|----|----|
|尝试非阻塞低获取锁 | 当前线程尝试获取锁，如果这一时刻锁没有被其他线程获取到，则成功获取并持有锁 |
|能被中断低获取锁| 与synchronized不同，获取到锁的线程能够相应中断，当获取到锁的线程被中断时，中断异常将会被抛出，同时锁会被释放|
|超时获取锁|在制定的截止时间之前获取锁，如果截止时间到了仍旧无法获取锁，则返回|

##### Lock的API

|方法名称|描 述|
|---|---|
| void lock()|获取锁，调用该方法当前线程将会获取锁，当锁获得后，从该方法返回|
| void lockInterruptibly() throws InterrupedException| 可中断地获取锁，和lock()方法的不同之处在于该方法会响应中断，即在锁的获取中可以中断当前线程|
| boolead tryLock() | 尝试非阻塞的获取锁，调用该方法后立刻返回，如果能够获取则返回true，否则返回false|
| boolean tryLock(Long time,TimeUnit unit) throws InterrupedException | 超市的获取锁，当前线程在一下3中情况下回返回 1、当前线程在超市时间内获得了锁 2、当前线程在超市时间内被中断 3、超市时间结束，返回false|
| void unlock() | 释放锁|
| Condition newCondition() | 获取等待通知组件，该组件和当前的锁绑定，当前线程只有获得了锁，才能调用该组件的wait()方法，而调用后，当前线程将释放锁|

##### 队列同步器 
AbstractQueuedSynchronizer和常用的ReentrantLock
Lock接口的实现基本都是通过聚合了一个同步器的子类来完成线程访问控制的
同步器的设计是基于模板方法模式

##### 重入锁
ReentrantLock

##### 读写锁
Mutex和ReentrantLock基本都是排它锁，这些锁在同一时刻只允许一个线程进行访问
而读写锁在同一时刻可以允许多个读线程访问，但是在写线程访问时，所有的读线程和其他写
线程均被阻塞。读写锁维护了一堆锁，一个读锁和一个写锁，通过分离读锁和写锁，使得并发性相比一半的排他锁有了很大提升。




##### 13个原子操作类 `atomicdemo`
从JDK 1.5开始提供了java.util.concurrent.atomic包，这个包中的原子操作类提供了一种用法简单、
性能高效、线程安全地更新一个变量的方式。
//TODO
JDK 1.8有待更新

###### 原子更新基本类型类
AtomicBoolean：原子更新布尔类型
AtomicInteger：原子更新整型
AtomicLong：原子更新长整型

常用方法

| 方法 | 说明 |
|-----|-----|
|int addAdnGet(int delta) | 以原子方式将输入的数值与实例中的值(AtomicInteger里的value)相加，并返回结果|
| boolean compareAndSet(int expect,int update)  | 如果输入的数值等于语气值，则以原子方式将该值设置为输入的值 |
| int getAndIncrement() | 以原子方式将当前值加1，注意，这里返回的是自增前的值|
| void lazySet(int newValue) | 最终会设置成newValue，使用设置值后，可能导致其他线程在之后的一小段时间内还是可以读到旧的值|
| int getAndSet | 以原子方式设置为newValue的值，并返回旧值 |

###### 原子更新数组
> AtomicIntegerArray：原子更新整型数组里的元素
AtomicLongArray：原子更新长整型数组里的元素
AtomicReferenceArry：原子更新引用类型数组里的元素
AtomicIntegerArray类主要是提供原子的方式更新数组里的整型，其常用方法如下：
>- int addAndGet(int i,int delta)：以原子方式将输入值与数组中索引i的元素相加
>- boolean compareAndSet(int i,int expect,int update)：如果当前值等于预期值，则以原子方式将数组位置i的元素设置成update值

实例：`AtomicIntegerArrayTest.java` 

###### 原子更新引用类型
原子更新基本类型的AtomicInteger，只能更新一个变量，如果要原子更新多个变量，就需要使用原子更新引用类型提供的类。
有3个类：
>- AtomicReference：原子更新引用类型
>- AtomicReferenceFieldUpdater：原子更新引用类型里的字段
>- AtomicMarkableReference：原子更新带有标记位的引用类型。可以原子更新一个布尔类型的标记位和引用类型。

示例：`AtomicReferenceTest.java`

###### 原子更新字段类
```
如果需要原子地更新某个类里的某个字段时，就需要使用原子更新字段类
```
3个类可以进行原子字段更新：
>- AtomicIntegerFieldUpdater：原子更新整型的字段的更新器
>- AtomicLongFieldUpdater：原子更新长整型字段的更新器
>- AtomicStampedReference：原子更新带有版本号的引用类型。该类型将整数值与引用关联起来，可用于原子的更新数据和数据的版本号，可以解决使用CAS进行原子更新时可能出现的ABA问题

示例：`AtomicIntegerFieldUpdaterTest`

##### Java中的并发工具类 `concurrent_tools`
###### 等待多线程完成的CountDownLatch
构造一个int类型的计数器，使用await()方法等待线程，每完成一个线程countDown一次，计数器-1，直到0则往下继续运行，否则一直等待。
我们不可能让主线程一直等待，所以可以使用await(long time,TimeUnit unit)设置超时时间

`正常使用join()方法等待线程完成，join()实现原理是不停检查join线程是否存活，如果join线程存活则让当前线程永远等待。其中wait(0)表示永远等待下去
直到join线程终止后，线程的this.notifyAll()方法会被调用，调用notifyAll()方法是JVM里实现的，需要查看JVM源码`

示例：`CountDownLatchTest.java`

###### 同步屏障CyclicBarrier
可循环使用的屏障，让一组线程到达一个屏障（也可以叫同步点）时被阻塞，直到最后一个线程到达屏障时，屏障才会开门，所有被屏障拦截的线程才会继续运行
`每个线程调用await()方法告诉CyclicBarrier我已经到达了屏障，让后当前线程被阻塞`

示例：`CycliBarrierTest.java`

CyclicBarrier还提供了一个更高级的构造函数CyclicBarrier(int parties,Runnable barrierAction)，用于在线程达到屏障时，优先执行barrierAction，方便处理更复杂的业务场景。
- await() 返回值为当前线程的索引，0表示当前线程是最后一个到达的线程
- await(long timeout, TimeUnit unit)  在await()的基础上增加超时机制，如果超出指定的等待时间，则抛出 TimeoutException 异常。如果该时间小于等于零，则此方法根本不会等待。
- isBroken() 在异常区域获取当前线程是否被终止，返回boolen类型
- getNumberWaiting() 可以获取waiting状态的线程数量
- reset() 将屏障重置为其初始状态。如果所有参与者目前都在屏障处等待，则它们将返回，同时抛出一个BrokenBarrierException。

示例：`CycliBarrierTest2.java`

应用场景：
可以用于多线程计数数据，最后合并计算结果的场景。例如，用一个Excel保存了用户所有银行流水，每个Sheet的日均银行流水，最后，再用barrierAction用这些线程的计算结果，计算出整个Excel的日均银行流水。
示例：`BankWaterService.java`

####### CountDownLatch和CycliBarrier的区别
CountDownLatch的计数器智能使用一次，而CycliBarrier的计数器可以使用reset()方法重置。
所以CyclicBarrier能处理更为复杂的业务场景。例如，如果计算发生错误，可以重新计数器，并让线程重新执行一次。

##### 控制并发线程数的Semaphore
```Semaphore(信号量)是用来控制同时访问特定资源的线程数量，它通过协调各个线程，以保证合理的使用公共资源。```
1.应用场景
Semaphore可以用作流量控制，特别是公用资源有限的应用场景，比如数据库连接。

|方法|说明|
|---|---|
| acquire() | 获得许可证，线程可以进行执行|
| tryAcquire() | 尝试获取许可证 |
| relase() | 释放许可证 |
| intavailablePermits() | 返回此信号量中当前可用的许可证数 |
| intgetQueueLength() | 返回正在等待获取许可证的线程数 |
| booleanhasQueuedThreads() | 是否有线程正在等待获取许可证 |
| void reducePermits(int reduction) | 减少reduction个许可证，是个protected方法 |
| Collection getQueuedThreads() | 返回所有等待获取许可证的线程集合，是个protected方法 |
示例：`SemaphoreTest.java`

##### 线程间交换数据的Exchanger
```
Exchanger(交换者)是一个用于线程间协作的工具类。用于进行线程间数据交换。它提供一个同步点，在这个同步点，两个线程可以交换彼此的数据。
这两个线程通过exchange方法交换数据，如果第一个线程先执行exchange()方法，它会一直等待第二个线程也执行exchange()方法。当两个线程到达同步点时，
这两个线程就可以交换数据，将本线程生产出来的数据传递给对方。
```
1.应用场景
可以用于遗传算法，遗传算法里需要选出两个人作为交配对象，这时候会交换两人的数据，
并使用交叉规则得出2个交配结果。
也可以用于校对工作，比如我们需要将纸制银行流水通过人工的方式录入成电子银行流水，
为了避免错误，采用AB岗两人进行录入，录入到Excel之后，系统需要加载这两个Excel，
并对两个Excel数据进行校对，看看是否录入一致。

示例：`ExchangerTest.java`

#### Java中的线程池
```Java中的线程池是运用场景最多的并发框架，几乎所有需要异步或并发执行任务的 程序都可以使用线程池。```
在开发过程中，合理地使用线程池能够带来3个好处
- 第一：降低资源消耗。通过重复利用已创建的线程降低线程创建和销毁造成的消耗
- 第二：提高响应速度。当任务到达时，任务可以不需要等到线程创建就能立即执行
- 第三：提高线程的课管理性。线程是稀缺资源，如果无限制地创建，不仅会消耗系统资源，
还会降低系统的稳定性，使用线程池可以进行统一分配，调优和监控。但是，要做到合理利用线程池，必须对其实现原理了如指掌

###### 线程池的实现原理