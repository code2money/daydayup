package com.lecoboy.highconcurrency.atomicdemo;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * 原子更新数组 示例
 */
public class AtomicIntegerArrayTest {
    static int[] value = new int[]{1, 2};
    //AtomicIntegerArray会在复制一份数组，所以对内部元素修改不会影响传入数组
    static AtomicIntegerArray ai = new AtomicIntegerArray(value);

    public static void main(String[] args) {
        ai.getAndSet(0, 3);
        System.out.println(ai.get(0));
        System.out.println(value[0]);
    }
}
