package com.lecoboy.highconcurrency.atomicdemo;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * 原子更新字段类 示例
 * 第一步：
 * 因为原子更新字段类都是抽象类，每次使用的时候必须使用静态方法newUpdater()创建一个更新器，并且需要设置
 * 想要更新的类和属性
 * 第二步：
 * 更新类的字段（属性）必须使用public valatile修饰符
 */
public class AtomicIntegerFieldUpdaterTest {
    //创建原子更新器，并设置需要更新的对象类和对象的属性
    private static AtomicIntegerFieldUpdater afu = AtomicIntegerFieldUpdater.newUpdater(User.class, "old");

    public static void main(String[] args) {
        //设置柯南的年龄是10岁
        User conan = new User("conan", 10);
        //柯南长了一岁，但是仍然会输出旧的年龄
        System.out.println(afu.getAndIncrement(conan));
        //输出柯南现在的年龄
        System.out.println(afu.get(conan));
    }

    public static class User {
        private String name;
        //字段必须为public valatile 修饰
        public volatile int old;

        public User(String name, int old) {
            this.name = name;
            this.old = old;
        }

        public String getName() {
            return name;
        }

        public int getOld() {
            return old;
        }
    }
}
