package com.lecoboy.highconcurrency.concurrent_tools;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 并发工具CountDownLatch测试类
 */
public class CountDownLatchTest {
    static CountDownLatch countDownLatch = new CountDownLatch(2);

    public static void main(String[] args) throws InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(1);
                countDownLatch.countDown();
                System.out.println(2);
                countDownLatch.countDown();
            }
        }).start();
        System.out.println(3);
        //await会阻塞主线程 直到countDown 计数器为0
        countDownLatch.await();
        System.out.println(4);

    }
}
