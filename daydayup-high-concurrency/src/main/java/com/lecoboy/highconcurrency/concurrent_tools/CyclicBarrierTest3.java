package com.lecoboy.highconcurrency.concurrent_tools;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 测试 reset方法示例 TODO 不够完整 需要重新编辑测试
 */
public class CyclicBarrierTest3 {

    public static void main(String[] args) throws InterruptedException {
        ThreadPoolExecutor service = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);

        CyclicBarrier cyclicBarrier = new CyclicBarrier(3, () -> {
            System.out.println("全都到了 【开吃!】");
        });
        for (int i = 0; i < 3; i++) {
            final int number = i;
            service.execute(() -> {
                try {

                    System.out.println("编号:" + number + "开始出发 【去聚餐】");
                    Thread.sleep((number + 1) * 1000);
                    System.out.println("编号:" + number + " 【到达聚餐地点】");
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        Thread.sleep(1 * 1000);
        System.out.println(cyclicBarrier.getNumberWaiting());
        //cyclicBarrier.reset();
        System.out.println(cyclicBarrier.getNumberWaiting());
    }
}
