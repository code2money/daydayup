package com.lecoboy.highconcurrency.connectionDemo;

import java.sql.Connection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class ConnectionPool {
    //双向链表结构定义连接集合
    //双向链表结构链接集合
    private LinkedList<Connection> pool = new LinkedList<Connection>();

    /**
     * 初始化连接数
     *
     * @param initialSize 初始化数量
     */
    public ConnectionPool(int initialSize) {
        if (initialSize > 0) {
            for (int i = 0; i < initialSize; i++) {
                pool.addLast(ConnectionDriver.createConnection());
            }
        }
    }

    /**
     * 释放连接
     * @param connection
     */
    public void releaseConnection(Connection connection) {
        if (connection != null) {
            synchronized (pool) {
                //连接释放后需要进行通知，这样其他消费则能够感知到连接池中已经归还了一个连接
                //System.out.println("增加一个");
                pool.addLast(connection);
                pool.notifyAll();
            }
        }
    }

    /**
     * 批量连接
     * 在mills内无法获取到连接，将会返回null
     *
     * @param mills 超时时间 毫秒
     * @return
     * @throws InterruptedException
     */
    public Connection fetchConnection(Long mills) throws InterruptedException {
        synchronized (pool) {
            //完全超时
            if (mills <= 0) {
                while (pool.isEmpty()) {
                    pool.wait();
                }
                return pool.removeFirst();
            } else {
                //超时计算

                //当前时间+超时时间
                long future = System.currentTimeMillis() + mills;
                long remaining = mills;
                //如果集合为空 并且 超时时间大于0
                //保护机制 如果集合为空则等待超时
                while (pool.isEmpty() && remaining > 0) {
                    //模拟超时 等待时间和超时时间一致
                    //System.out.println("进入超时");

                    pool.wait(remaining);
                    remaining = future - System.currentTimeMillis();
                }
                Connection result = null;
                if (!pool.isEmpty()) {
                    result = pool.removeFirst();
                }
                return result;
            }
        }
    }
}
