package com.lecoboy.highconcurrency.connectionDemo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class ConnectionPoolTest {
    static ConnectionPool pool = new ConnectionPool(10);
    //保证所有ConnectionRunner能够同时开始
    static CountDownLatch startCountDown = new CountDownLatch(1);
    //main线程将会等待所有ConnectionRunner结束后才能继续执行
    static CountDownLatch endCountDown;

    public static void main(String[] args) throws InterruptedException {
        //线程数量，可以修改线程数量进行观察
        int threadCount = 1000;
        endCountDown = new CountDownLatch(threadCount);

        //循环次数
        int count = 20;
        //获取连接数
        AtomicInteger got = new AtomicInteger();
        //未获取到连接数
        AtomicInteger notGot = new AtomicInteger();
        for (int i = 0; i < threadCount; i++) {
            Thread thread = new Thread(new ConnectionRunner(count, got, notGot), "ConnectionRunnerThread");
            thread.start();
        }

        startCountDown.countDown();
        //阻塞等待执行完成
        endCountDown.await();
        System.out.println("total invoke：" + (threadCount * count));
        System.out.println("got connection: " + got);
        System.out.println("notGot connection: " + notGot);


    }

    static class ConnectionRunner implements Runnable {
        int count;
        //计数器
        AtomicInteger got;
        AtomicInteger notGot;

        public ConnectionRunner(int count, AtomicInteger got, AtomicInteger notGot) {
            this.count = count;
            this.got = got;
            this.notGot = notGot;
        }

        @Override
        public void run() {
            try {
                startCountDown.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //每个线程循环次数
            while (count > 0) {
                try {
                    //从线程池中获取连接，如果100ms内无法获取到，将会返回null
                    //分别统计连接获取的数量got 和未获取到的数量notgot
                    Connection connection = pool.fetchConnection(100L);

                    if (connection != null) {
                        try {
                            connection.createStatement();
                            connection.commit();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        } finally {
                            pool.releaseConnection(connection);
                            got.incrementAndGet();
                        }
                    } else {
                        notGot.incrementAndGet();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    count--;
                }
            }
            //减少个数 位置一定要在循环外层
            endCountDown.countDown();
        }
    }

}
