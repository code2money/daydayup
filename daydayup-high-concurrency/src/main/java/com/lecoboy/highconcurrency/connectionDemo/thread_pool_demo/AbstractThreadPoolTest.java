package com.lecoboy.highconcurrency.connectionDemo.thread_pool_demo;


import java.util.concurrent.atomic.AtomicInteger;

/**
 * 日志打点 测试类 可有可无
 * <p>
 * 为了统计线程执行成功和失败次数
 */
public abstract class AbstractThreadPoolTest {
    //成功计数器
    private AtomicInteger successCountI = new AtomicInteger();
    //失败计数器
    private AtomicInteger errorCountI = new AtomicInteger();
    //共计工作者
    private AtomicInteger workerI = new AtomicInteger();

    //进入run线程个数
    private AtomicInteger runI = new AtomicInteger();

    public void icrSuccessCount() {
        //successCountI.incrementAndGet();
        safeCount(successCountI);
    }

    public void icrErrorCount() {
        errorCountI.incrementAndGet();
    }

    public void icrWorker() {
        workerI.incrementAndGet();
    }

    public void icrRun() {
        runI.incrementAndGet();
    }

    public void getExecteResult() {
        System.out.println("工作者个数：" + workerI.get());
        System.out.println("进入run个数：" + runI.getAndIncrement());
        System.out.println("成功个数：" + successCountI.getAndIncrement());

        System.out.println("失败个数：" + errorCountI.getAndIncrement());
    }

    public void safeCount(AtomicInteger count){
        for (;;){
            int i=count.get();
            boolean suc = count.compareAndSet(i,++i);
            if(suc){
                break;
            }
        }
    }
}
