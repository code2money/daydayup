package com.lecoboy.highconcurrency.connectionDemo.thread_pool_demo;

/**
 * 简单的线程池接口
 *
 * @param <Job>
 */
public interface ThreadPool<Job extends Runnable> {
    //执行一个job，这个job需要实现Runnable
    void execute(Job job);

    //关闭线程池
    void shutdown();

    //增加工作者线程
    void addWorkers(int num);

    //减少工作者线程
    void removeWorker(int num);

    //得到正在等待执行的任务数量
    int getJobSize();


    //***********test 可有可无***********
    void getExecteResult();
}
