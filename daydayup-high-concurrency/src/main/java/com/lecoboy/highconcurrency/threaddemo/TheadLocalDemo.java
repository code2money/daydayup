package com.lecoboy.highconcurrency.threaddemo;

import java.util.concurrent.TimeUnit;

/**
 * TheadLocal可以被使用在调用好使统计的功能上，在方法的入口前执行begin()方法
 * 在方法调用后执行en()方法，好处是两个方法的调用不用再一个方法或类中，比如在AOP中
 * 可以在方法调用前的切入点执行begin()方法，在方法调用后执行end()这样依旧可以获得方法的执行耗时
 */
public class TheadLocalDemo {
    //第一次get()方法调用时会进行初始化（如果set方法没有调用)，每个线程会调用一次
    private static final ThreadLocal<Long> TIME_THERADLOCAL = new ThreadLocal<Long>(){
        protected Long initialValue(){
            return System.currentTimeMillis();
        }
    };

    public static final void begin(){
        TIME_THERADLOCAL.set(System.currentTimeMillis());
    }

    public static final long end(){
        return System.currentTimeMillis() - TIME_THERADLOCAL.get();
    }

    public static void main(String[] args) throws InterruptedException {
        TheadLocalDemo.begin();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("Cost:"+TheadLocalDemo.end()+" mills");
    }

}
