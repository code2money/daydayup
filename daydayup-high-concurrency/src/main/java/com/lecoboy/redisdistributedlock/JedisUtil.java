package com.lecoboy.redisdistributedlock;

import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Service
public class JedisUtil {
    public static Jedis getJedis(){
        Jedis jedis = new Jedis("10.32.16.22");
        return jedis;
    }
}
