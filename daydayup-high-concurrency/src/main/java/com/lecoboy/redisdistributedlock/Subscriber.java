package com.lecoboy.redisdistributedlock;

import redis.clients.jedis.JedisPubSub;

import java.util.concurrent.CountDownLatch;

public class Subscriber extends JedisPubSub {
    private CountDownLatch countDownLatch;
    private Thread thread;

    public Subscriber(CountDownLatch countDownLatch, Thread thread) {
        this.countDownLatch = countDownLatch;
        this.thread = thread;
    }

    //监听收到消息
    //取得订阅消息后，通过countDownLatch唤醒被阻塞的线程，再次尝试获得锁
    @Override
    public void onMessage(String channel, String message) {
        //收到广播
        System.out.println(thread.getName() + "收到广播" + message);
        countDownLatch.countDown();
    }
}
