### RocketMQ学习
[](https://www.jianshu.com/p/2838890f3284)

## 一、MQ背景&选型
消息队列做为高并发系统核心组件之一，能够帮助业务系统结构提升开发效率和系统稳定性。
主要具有以下优势：
- 学风填谷（主要解决瞬时写压力大于应用服务能力导致消息丢失、系统崩溃等问题）
- 系统解耦（结局不同重要程度、不同能力级别系统之间以来导致一死全死）
- 提升性能（当存在一对多调用时，可以发一条消息给消息系统，让消息系统通知相关系统）
- 续流压测（线上有些链路不好压测，可以通过堆积一定量消息再放开来压测）

目前主流的MQ是RocketMQ、kafka、RabbitMQ，RocketMQ相比于RabbitMQ、Kafka具有主要优势特性有：
- 支持事务型消息（消息发送和DB操作保持两房的最终一致性，rabbitmq和kafka不支持）
- 支持结合rocketMQ的多个系统之间的数据最终一致性（多方事务，二方事务是前提）
- 支持18个级别的延迟消息（RabbitMQ和Kafka不支持）
- 支持制定次数和时间间隔的失败消息重发（kafka不支持，RabbitMQ需要手动确认）
- 支持consumer端tag过滤，减少不必要的网络传输(RabbitMQ和Kafka不支持)
- 支持重复消费（RabbitMQ不支持,kafka支持）
详细对比：
![sd](https://upload-images.jianshu.io/upload_images/12619159-ebd12b24d5ae33d9.png)

## 二、RocketMQ集群概述
### 1.RocketMQ集群部署结构
![集群部署架构图](https://upload-images.jianshu.io/upload_images/12619159-a858d38e0b38c406.png)
#### 1)Name Server（协调者）
Name Server是一个几乎无状态节点，可集群部署，节点之间无任何信息同步。
#### 2)Broker(MQ消息服务器，中转角色，用于消息存储与生产消费转发)
Broker部署相对复杂，Broker分为Master和Slave，一个Master可以对应多个Slave，但
是一个Slave只能对应一个Master,Master和Slave的对应关系通过制定相同的Broker Name，
不同的Broker Id来定义，BrokerId为0表示Master，非0表示Slave。Master也可以部署多个。

每个Broker和Name Server集群中的所有节点建立长链接，定时（每隔30s)注册Topic信息到所有Name Server。
Name Server定时（每隔10s）扫描所有存活的Broker的连接，如果Name Server超过2分钟没有收到心跳，则Name Server断开与Broker的连接。

#### 3) Producer（生产者）
Producer与Name Server集群中的其中一个节点（随机选择）建立长连接，定期从Name Server取
Topic路由信息，并向提供Topic服务的Master建立长连接，且定时向Master发送心跳。Producer完全无状态，可集群部署。

Producer每隔30s(由ClientConfig和pollNameServerInterval)从Name Server获取所有Topic队列的最新情况，
这意味着如果Broker不可用，Producer最多30s能够感知，在此期间内发往Broker的所有消息都会失败。

Producer每隔30s(由ClientConfig中heartbeatBrokerInterval决定)向所有关联的Broker发送心跳，
Broker每隔10s中扫描所有存活的连接，如果Broker在2分钟内没有收到心跳数据，则关闭与Producer的连接。

#### 4）Consumer（消费者）
Consumer与Name Server集群中的其中一个节点（随机选择）建立长连接，定期从Name Server取Topic路由信息，并向提供Topic服务的Master、Slave建立长连接，
且定时向Master、Slave发送心跳。Consumer即可以从Master订阅消息，也可以从Slave订阅消息，订阅规则由Broker配置决定。

Consumer每隔30s从Name Server获取Topic的最新队列情况，这意味着Broker不可用时,Consumer最后最多需要30s才能感知。

Consumer每隔30s（由ClinetConfig中heartbeatBrokerInterval决定）向所有关联的Broker发送心跳，
Broker每隔10s扫描所有存活的连接，若某个连接2分钟内没有发送心跳数据，则关闭连接；并向该Consumer Group的所有Consumer发出通知，
Group内的Consumer重新分配队列，然后继续消费。

当Consumer得到master当即通知后，专项Slave消费，Slave不能保证master的消息100%都同步过来，
因此会有少量的消息丢失。但是一旦mster恢复，未同步过去的消息会被最终消费掉。

消费者队列是消费者连接之后（或者之前有连接过）才创建的。我们将原生的消费者标识{IP}@{消费者Group}拓展为{IP}@{消费者Group}{topic}{tag}，
例如：（例如xxx.xxx.xxx.xxx@mqtest_producer-group_2m2sTest_tag-zyk）。
任何一个元素不同，都认为是不同的消费端，每隔消费端会拥有一份自己消费队列（默认是Broker队列数量*Broker数量）。新挂在的消费者队列中拥有commitlog中的所有数据。


### 三、RokderMQ如何支持分布式事务消息
####场景
A（存在DB操作）、B（存在DB操作）两方需要保证分布式事务一致性，通过引入中间层MQ，A和MQ保持事务一致性（异常情况下通过MQ反查A接口实现check），
B和MQ保证事务一致（通过重试），从而达到最终事务一致性。

原理：大事务 = 小事务 + 异步

#### 1.MQ与DB一致性原理（两方事务）
流程图
![流程图](https://upload-images.jianshu.io/upload_images/12619159-6f4f6754d6f02058.png)
上图是RocketMQ提供的保证MQ消息、DB事务一致性的方案。
MQ消息、DB操作一致性方案：
1）发送消息到MQ服务器，此时消息状态为SEND_OK。此消息为consumer不可见。

2）执行DB操作；Db操作成功commit DB操作，DB执行失败Rolllback DB操作。

3）如果DB执行成功，回复MQ服务器，将状态为COMMIT_MESSAGE；如果DB执行失败，
回复MQ服务器，将状态改为ROLLBACK_MESSAGE。注意此过程有可能失败。

4）MQ内部提供一个名为"事务状态服务"的服务，此服务会检查事务消息的状态，如果发现
消息未COMMIT，则通过Producer启动时注册的TransationCheckListenter来回调业务系统，
业务系统在checkLocalTransactionState方法中间差DB事务状态，如果成功，则回复COMMIT_MESSAGE，否则回复ROLLBACK_MESSAGE.

说明：

上边以DB为例，其实此处可以是任务业务或者数据源。

以上SEND_OK,COMMIT_MESSAGE、ROLLBACK_MESSAGE均为client jar提供的状态，在MQ服务器内部是一个数字。

TransactionCheckListener是在消息的commit或者rollback消息丢失的情况下才会回调（上图中灰色部分）。
这种消息丢失支存在于断网或者rocketMQ集群挂了的情况下。当RocketMQ集群挂了，如果采用异步刷盘，存在1s内数据丢失风险，
异步刷盘场景下保证事务没有意义。所以如果要核心业务用RocketMQ解决分布式事务问题，建议选择同步刷盘模式。

#### 2.多系统之间数据一致性（多方事务）
![多系统之间数据一致性](https://upload-images.jianshu.io/upload_images/12619159-cb4ce1a4c8b79fb1.png)

当需要保证多方（超过2方）的分布式一致性，上面的两方事务一致性（通过RocketMq的事务性消息解决）
已经无法支持。这个时候需要引入TCC模式思想（Try-Confirm-Cancel）

以上图交易系统为例：
1）交易系统创建订单（往Db插入一条记录），同时发送订单创建消息。通过RocketMQ事务性消息保证一致性。

2）接着执行完成订单所需的同步核心RPC服务（非核心的系统通过监听MQ消息自行处理，处理结果不会影响交易状态）。执行成功更改订单状态，同时发送MQ消息。

3）交易系统接收自己发送的订单创建消息，通过定时调度系统创建延时回滚任务（或者使用RocketMQ的重试功能，设置第二次发送消息
为定时任务的延迟创建消息。在非消息堵塞的情况下，消息第一次到达延迟为1ms左右，这时可能RPC还未执行完，订单状态还未设置为完成，第二次消费时间可以指定）。
延迟任务先通过查询订单状态判断订单是否完成，完成则不创建回滚任务，否则创建。PS：多个RPC可以创建一个回滚任务，通过一个消费组接收一次消息就可以；也可以通过创建
多个消费组，一个消息消费多次，每次消费创建一个RPC的回滚任务。回滚任务失败，通过MQ的重发来重试。

以上是交易系统和其他系统之间保持最终一致性的解决方案。

#### 3.案例分析
##### 1）单机环境下的

未完待续...