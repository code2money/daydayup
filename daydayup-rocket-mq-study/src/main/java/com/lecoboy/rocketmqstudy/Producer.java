package com.lecoboy.rocketmqstudy;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.concurrent.TimeUnit;

public class Producer {
    public static void main(String[] args) {
        DefaultMQProducer producer = new DefaultMQProducer("Producer");
        producer.setNamesrvAddr("192.168.199.244:9876");
        try {
            producer.start();
            for (int i = 0; i < 100; i++) {
                TimeUnit.SECONDS.sleep(1);
                String keys = i+"keys";
                String push = "push";
                Message msg = new Message("PushTopic",
                        push,
                        keys,
                        "Just for test.".getBytes());
                try {
                    SendResult result = producer.send(msg);
                    System.out.println(result.toString());
                } catch (MQClientException e) {
                    e.printStackTrace();
                } catch (RemotingException e) {
                    e.printStackTrace();
                } catch (MQBrokerException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            producer.shutdown();
        }
    }
}
