package com.lecoboy.rocketmqstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RocketMqStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RocketMqStudyApplication.class, args);
    }

}

