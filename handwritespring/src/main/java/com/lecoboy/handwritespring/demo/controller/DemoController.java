package com.lecoboy.handwritespring.demo.controller;

import com.lecoboy.handwritespring.demo.services.IDemoService;
import com.lecoboy.handwritespring.spring.framework.annotation.MyAutoWirted;
import com.lecoboy.handwritespring.spring.framework.annotation.MyController;
import com.lecoboy.handwritespring.spring.framework.annotation.MyRequestMapping;
import com.lecoboy.handwritespring.spring.framework.annotation.MyRequestParam;

@MyController
@MyRequestMapping("/demo")
public class DemoController {
    @MyAutoWirted
    private IDemoService demoServices;

    @MyRequestMapping("/test")
    public String testUrl(@MyRequestParam(value = "name") String name, @MyRequestParam(value = "id") String id) {
        return demoServices.test(name, id);
    }
}
