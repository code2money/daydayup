package com.lecoboy.handwritespring.demo.services;

public interface IDemoService {
    String test(String name, String id);
}
