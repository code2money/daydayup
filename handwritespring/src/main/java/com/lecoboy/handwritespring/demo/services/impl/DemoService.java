package com.lecoboy.handwritespring.demo.services.impl;

import com.lecoboy.handwritespring.demo.services.IDemoService;
import com.lecoboy.handwritespring.spring.framework.annotation.MyServices;

@MyServices
public class DemoService implements IDemoService {

    @Override
    public String test(String name, String id) {
        System.out.printf("call success");
        return String.format("name:%s,id:%s welcome to hand write spring.", name, id);
    }
}
