package com.lecoboy.handwritespring.spring.framework.annotation;

import java.lang.annotation.*;

/**
 * 自动注入
 *
 * @author leon
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyAutoWirted {
    String value() default "";
}
