package com.lecoboy.handwritespring.spring.framework.annotation;


import java.lang.annotation.*;

/**
 * 页面交互
 * @author leon
 */
@Target({ElementType.TYPE}) //TYPE 类或接口
@Retention(RetentionPolicy.RUNTIME) //保留类型
@Documented
public @interface MyController {
    String value() default "";
}
