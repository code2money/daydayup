package com.lecoboy.handwritespring.spring.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * 请求url
 * @author leon
 */
@Target({ElementType.TYPE,ElementType.METHOD})
public @interface MyRequestMapping {
    String value() default "";
}
