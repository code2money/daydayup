package com.lecoboy.handwritespring.spring.framework.annotation;


import java.lang.annotation.*;

/**
 * 参数映射
 * @author leon
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyRequestParam {

    String value() default "";

    boolean required() default true;
}
