package com.lecoboy.handwritespring.spring.framework.beans;

/**
 * Bean包装类
 * @author leon
 */
public class MyBeanWrapper {

    //Bean实例化的真实对象
    private Object wrapperInstance;
    private Class<?> wrapperClass;

    public MyBeanWrapper(Object wrapperInstance) {
        this.wrapperInstance = wrapperInstance;
        this.wrapperClass = wrapperInstance.getClass();
    }

    public Object getWrapperInstance() {
        return wrapperInstance;
    }

    public Class<?> getWrapperClass() {
        return wrapperClass;
    }

}
