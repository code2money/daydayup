package com.lecoboy.handwritespring.spring.framework.webmvc.servlet;

import com.lecoboy.handwritespring.spring.framework.annotation.MyController;
import com.lecoboy.handwritespring.spring.framework.annotation.MyRequestMapping;
import com.lecoboy.handwritespring.spring.framework.context.MyApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 手写Spring IOC DI MVC简单流程
 */
public class MyDispatcherServlet extends HttpServlet {

    //应用上下文
    private MyApplicationContext context = null;

    //保存url和Method的对应关系
    private List<MyHandlerMapping> handlerMappings = new ArrayList<MyHandlerMapping>();
    private Map<MyHandlerMapping,MyHandlerAdapter> handlerAdapters = new HashMap<MyHandlerMapping,MyHandlerAdapter>();

    //模板引擎列表
    private List<MyViewResolver> viewResolvers = new ArrayList<MyViewResolver>();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //偷懒
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //6.根据url调用method
        try{
            doDispatch(req, resp);

        }catch (Exception e){
            e.printStackTrace();
            resp.getWriter().write("500 Exception, Detail: " + Arrays.toString(e.getStackTrace()));
        }
    }

    private void doDispatch(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        //1、根据url拿到对应的handler
        MyHandlerMapping handler = getHandler(req);
        if(null == handler){
            processDispatchResult(req,resp,new MyModelAndView("404"));
            return;
        }

        //2、根据HandlerMapping拿到HandlerAdapter
        MyHandlerAdapter ha = getHandlerAdapter(handler);

        //3、根据HandlerApdater拿到ModelAndView
        MyModelAndView mv = ha.handle(req,resp,handler);

        //4、ViewResolver 拿到View
        //将View 渲染成浏览器能够接收的结果   HTML字符串
        processDispatchResult(req,resp,mv);

    }

    private void processDispatchResult(HttpServletRequest req, HttpServletResponse resp, MyModelAndView mv) throws Exception {
        if(null == mv){return;}
        if(this.viewResolvers.isEmpty()){return;}

        for (MyViewResolver viewResolver : this.viewResolvers) {
            MyView view = viewResolver.resolveViewName(mv.getViewName());
            view.render(mv.getModel(),req,resp);
            return;
        }
    }

    private MyHandlerAdapter getHandlerAdapter(MyHandlerMapping handler) {
        if(this.handlerAdapters.isEmpty()){return null;}
        MyHandlerAdapter ha = this.handlerAdapters.get(handler);
        return ha;
    }

    private MyHandlerMapping getHandler(HttpServletRequest req) {
        if(this.handlerMappings.isEmpty()){return  null;}
        String url = req.getRequestURI();
        String contextPath = req.getContextPath();
        url = url.replaceAll(contextPath,"").replaceAll("/+","/");

        for (MyHandlerMapping handlerMapping : this.handlerMappings) {
            Matcher matcher = handlerMapping.getPattern().matcher(url);
            //url是用正则去匹配Controller中的配置信息
            if(!matcher.matches()){continue;}
            return handlerMapping;
        }
        return null;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {

        context = new MyApplicationContext(config.getInitParameter("contextConfigLocation"));

        //初始化mvc9大组件
        //HandlerMappint
        //HandlerAdapter
        //ViewResolver
        initStrategies(context);
        System.out.println("My Spring framework is init.");
    }

    private void initStrategies(MyApplicationContext context) {
        //初始化Url和Method对应关系
        initHandlerMappings(context);
        //初始化参数适配器
        initHandlerAdapter(context);
        //初始化视图转换器
        initViewResolver(context);
    }

    private void initViewResolver(MyApplicationContext context) {
        String templateRoot = context.getConfig().getProperty("templateRoot");
        String templateRootPath = this.getClass().getClassLoader().getResource(templateRoot).getFile();
        File templateRootDir = new File(templateRootPath);
        for (File file : templateRootDir.listFiles()) {
            //templateRoot
            //TODO
            this.viewResolvers.add(new MyViewResolver(file.getPath()));
        }
    }

    private void initHandlerAdapter(MyApplicationContext context) {
        for (MyHandlerMapping handlerMapping : handlerMappings) {
            this.handlerAdapters.put(handlerMapping,new MyHandlerAdapter());
        }
    }

    private void initHandlerMappings(MyApplicationContext context) {
        //获取所有BeanNames
        String[] beanNames = context.getBeanDefinitionNames();
        for (String beanName : beanNames) {
            Object instance = context.getBean(beanName);
            Class<?> clazz = instance.getClass();
            //初始化Controller注解
            if (!clazz.isAnnotationPresent(MyController.class)) {
                continue;
            }
            //保存注解类上面的@MyRequestMapping("/demo")
            String baseUrl = "";
            if (clazz.isAnnotationPresent(MyRequestMapping.class)) {
                MyRequestMapping requestMapping = clazz.getAnnotation(MyRequestMapping.class);
                baseUrl = requestMapping.value();
            }
            //默认获取所有public方法
            for (Method method : clazz.getMethods()) {
                if (!method.isAnnotationPresent(MyRequestMapping.class)) {
                    continue;
                }
                MyRequestMapping requestMapping = method.getAnnotation(MyRequestMapping.class);
                //防止有多个//或单/，进行正则替换
                //demoquery
                //  //demo//query
                String regex = ("/" + baseUrl + "/" + requestMapping.value().replaceAll("\\*",".*")).replaceAll("/+","/");
                Pattern pattern = Pattern.compile(regex);
                this.handlerMappings.add(new MyHandlerMapping(instance, method, pattern));
                System.out.println("Mapperd " + regex + "," + method);
            }

        }
    }
}
