package com.lecoboy.handwritespring.spring.framework.webmvc.servlet;

import java.io.File;

/**
 * 视图转换器，模板引擎
 * @author leon
 */
public class MyViewResolver {

    //.jsp   .vm    .ftl 默认给.html
    private final String DEAULT_TEMPLATE_SUFFX = ".html";

    private File templateRootDir;

    public MyViewResolver(String teamplateRoot){
        String templateRootPath = this.getClass().getClassLoader().getResource(teamplateRoot).getFile();
        templateRootDir = new File(templateRootPath);
    }

    public MyView resolveViewName(String viewName){
        if(null == viewName || "".equals(viewName.trim())){return  null;}

        viewName = viewName.endsWith(DEAULT_TEMPLATE_SUFFX) ? viewName : (viewName + DEAULT_TEMPLATE_SUFFX);
        File templateFile = new File((templateRootDir.getPath() + "/" + viewName).replaceAll("/+","/"));
        return new MyView(templateFile);
    }
}
